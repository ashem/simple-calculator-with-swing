import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.plaf.FontUIResource;
public class SimpleCalculator implements ActionListener {
    ArrayList<String> numbers = new ArrayList<>();
    Boolean isEqualClicked = false;
    JFrame jf;
    JLabel displayLabel;
    JButton cancelButton, percentageButton, divideButton, button7, button8, button9, multiplyButton, button4, button5, button6, button3, button2, button1, plusButton, button0, minusButton, decimalButton ,equalsButton;
    public SimpleCalculator() {
        //Creating the frame
        jf = new JFrame("Simple Calculator");
        jf.setVisible(true);
        jf.setLayout(null);
        //To stop execution when we close the frame
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setBounds(500, 100, 295, 469);
        jf.getContentPane().setBackground(Color.darkGray);
        //***Set Background color for the frame-------------------

        //Making the text display area
        displayLabel = new JLabel();
        //add the created label to the frame
        displayLabel.setBounds(0, 0, 292, 70);
        displayLabel.setForeground(Color.white);
        displayLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        displayLabel.setFont(displayLabel.getFont().deriveFont(30, 40));
        jf.add(displayLabel);

        //Creating buttons; First Row

        cancelButton = new JButton("AC");
        cancelButton.setBounds(3, 75, 143, 70);
        cancelButton.setFont(new FontUIResource(null, 30, 30));
        cancelButton.setOpaque(true);
        cancelButton.setBackground(Color.GRAY);
        cancelButton.setForeground(Color.white);
        cancelButton.setBorder(null);
        cancelButton.addActionListener(this);
        jf.add(cancelButton);

        percentageButton = new JButton("%");
        percentageButton.setBounds(149, 75, 70, 70);
        percentageButton.setFont(new FontUIResource(null, 30, 30));
        percentageButton.setOpaque(true);
        percentageButton.setContentAreaFilled(true);
        percentageButton.setBorderPainted(false);
        percentageButton.setFocusPainted(false);
        percentageButton.setBackground(Color.GRAY);
        percentageButton.setForeground(Color.white);
        percentageButton.addActionListener(this);
        jf.add(percentageButton);

        divideButton = new JButton("/");
        divideButton.setBounds(222, 75, 70, 70);
        divideButton.setFont(new FontUIResource(null, 30, 30));
        divideButton.setOpaque(true);
        divideButton.setContentAreaFilled(true);
        divideButton.setBorderPainted(false);
        divideButton.setFocusPainted(false);
        divideButton.setBackground(Color.orange);
        divideButton.setForeground(Color.white);
        divideButton.addActionListener(this);
        jf.add(divideButton);

        // Second row

        button7 = new JButton("7");
        button7.setBounds(3, 148, 70, 70);
        button7.setFont(new FontUIResource(null, 30, 30));
        button7.setOpaque(true);
        button7.setContentAreaFilled(true);
        button7.setBorderPainted(false);
        button7.setBackground(Color.GRAY);
        button7.setForeground(Color.white);
        button7.addActionListener(this);
        jf.add(button7);

        button8 = new JButton("8");
        button8.setBounds(76, 148, 70, 70);
        button8.setFont(new FontUIResource(null, 30, 30));
        button8.setOpaque(true);
        button8.setContentAreaFilled(true);
        button8.setBorderPainted(false);
        button8.setFocusPainted(false);
        button8.setBackground(Color.GRAY);
        button8.setForeground(Color.white);
        button8.addActionListener(this);
        jf.add(button8);

        button9 = new JButton("9");
        button9.setBounds(149, 148, 70, 70);
        button9.setFont(new FontUIResource(null, 30, 30));
        button9.setOpaque(true);
        button9.setContentAreaFilled(true);
        button9.setBorderPainted(false);
        button9.setFocusPainted(false);
        button9.setBackground(Color.GRAY);
        button9.setForeground(Color.white);
        button9.addActionListener(this);
        jf.add(button9);

        multiplyButton = new JButton("x");
        multiplyButton.setBounds(222, 148, 70, 70);
        multiplyButton.setFont(new FontUIResource(null, 30, 30));
        multiplyButton.setOpaque(true);
        multiplyButton.setContentAreaFilled(true);
        multiplyButton.setBorderPainted(false);
        multiplyButton.setFocusPainted(false);
        multiplyButton.setBackground(Color.orange);
        multiplyButton.setForeground(Color.white);
        multiplyButton.addActionListener(this);
        jf.add(multiplyButton);

        // Third row

        button4 = new JButton("4");
        button4.setBounds(3, 221, 70, 70);
        button4.setFont(new FontUIResource(null, 30, 30));
        button4.setOpaque(true);
        button4.setContentAreaFilled(true);
        button4.setBorderPainted(false);
        button4.setBackground(Color.GRAY);
        button4.setForeground(Color.white);
        button4.addActionListener(this);
        jf.add(button4);

        button5 = new JButton("5");
        button5.setBounds(76, 221, 70, 70);
        button5.setFont(new FontUIResource(null, 30, 30));
        button5.setOpaque(true);
        button5.setContentAreaFilled(true);
        button5.setBorderPainted(false);
        button5.setFocusPainted(false);
        button5.setBackground(Color.GRAY);
        button5.setForeground(Color.white);
        button5.addActionListener(this);
        jf.add(button5);

        button6 = new JButton("6");
        button6.setBounds(149, 221, 70, 70);
        button6.setFont(new FontUIResource(null, 30, 30));
        button6.setOpaque(true);
        button6.setContentAreaFilled(true);
        button6.setBorderPainted(false);
        button6.setFocusPainted(false);
        button6.setBackground(Color.GRAY);
        button6.setForeground(Color.white);
        button6.addActionListener(this);
        jf.add(button6);

        minusButton = new JButton("-");
        minusButton.setBounds(222, 221, 70, 70);
        minusButton.setFont(new FontUIResource(null, 30, 30));
        minusButton.setOpaque(true);
        minusButton.setContentAreaFilled(true);
        minusButton.setBorderPainted(false);
        minusButton.setFocusPainted(false);
        minusButton.setBackground(Color.orange);
        minusButton.setForeground(Color.white);
        minusButton.addActionListener(this);
        jf.add(minusButton);

        // Fourth row

        button1 = new JButton("1");
        button1.setBounds(3, 294, 70, 70);
        button1.setFont(new FontUIResource(null, 30, 30));
        button1.setOpaque(true);
        button1.setContentAreaFilled(true);
        button1.setBorderPainted(false);
        button1.setBackground(Color.GRAY);
        button1.setForeground(Color.white);
        button1.addActionListener(this);
        jf.add(button1);

        button2 = new JButton("2");
        button2.setBounds(76, 294, 70, 70);
        button2.setFont(new FontUIResource(null, 30, 30));
        button2.setOpaque(true);
        button2.setContentAreaFilled(true);
        button2.setBorderPainted(false);
        button2.setFocusPainted(false);
        button2.setBackground(Color.GRAY);
        button2.setForeground(Color.white);
        button2.addActionListener(this);
        jf.add(button2);

        button3 = new JButton("3");
        button3.setBounds(149, 294, 70, 70);
        button3.setFont(new FontUIResource(null, 30, 30));
        button3.setOpaque(true);
        button3.setContentAreaFilled(true);
        button3.setBorderPainted(false);
        button3.setFocusPainted(false);
        button3.setBackground(Color.GRAY);
        button3.setForeground(Color.white);
        button3.addActionListener(this);
        jf.add(button3);

        plusButton = new JButton("+");
        plusButton.setBounds(222, 294, 70, 70);
        plusButton.setFont(new FontUIResource(null, 30, 30));
        plusButton.setOpaque(true);
        plusButton.setContentAreaFilled(true);
        plusButton.setBorderPainted(false);
        plusButton.setFocusPainted(false);
        plusButton.setBackground(Color.orange);
        plusButton.setForeground(Color.white);
        plusButton.addActionListener(this);
        jf.add(plusButton);

        // Fifth row

        button0 = new JButton("0");
        button0.setBounds(3, 367, 143, 70);
        button0.setFont(new FontUIResource(null, 30, 30));
        button0.setOpaque(true);
        button0.setContentAreaFilled(true);
        button0.setBorderPainted(false);
        button0.setBackground(Color.GRAY);
        button0.setForeground(Color.white);
        button0.addActionListener(this);
        jf.add(button0);

        decimalButton = new JButton(".");
        decimalButton.setBounds(149, 367, 70, 70);
        decimalButton.setFont(new FontUIResource(null, 30, 30));
        decimalButton.setOpaque(true);
        decimalButton.setContentAreaFilled(true);
        decimalButton.setBorderPainted(false);
        decimalButton.setFocusPainted(false);
        decimalButton.setBackground(Color.GRAY);
        decimalButton.setForeground(Color.white);
        decimalButton.addActionListener(this);
        jf.add(decimalButton);

        equalsButton = new JButton("=");
        equalsButton.setBounds(222, 367, 70, 70);
        equalsButton.setFont(new FontUIResource(null, 30, 30));
        equalsButton.setOpaque(true);
        equalsButton.setContentAreaFilled(true);
        equalsButton.setBorderPainted(false);
        equalsButton.setFocusPainted(false);
        equalsButton.setBackground(Color.orange);
        equalsButton.setForeground(Color.white);
        equalsButton.addActionListener(this);
        jf.add(equalsButton);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // Operation of cancelButton Button
        if (e.getSource() == cancelButton) {
            displayLabel.setText("");

            //Operation of percentage Button

        } else if (e.getSource() == percentageButton) {
            float perF = (float) (Float.parseFloat(displayLabel.getText()) * 0.01);
            String perS = Float.toString(perF);
            displayLabel.setText(perS);


        } else if (e.getSource() == button7) {
            // To avoid concatenation of the entered number post displaying the result
            if (isEqualClicked) {
                displayLabel.setText("7");
            } else {
                displayLabel.setText(displayLabel.getText() + "7");
            }

        } else if (e.getSource() == button8) {
            if (isEqualClicked) {
                displayLabel.setText("8");
            } else {
                displayLabel.setText(displayLabel.getText() + "8");
            }

        } else if (e.getSource() == button9) {
            if (isEqualClicked) {
                displayLabel.setText("9");
            } else {
                displayLabel.setText(displayLabel.getText() + "9");
            }

        } else if (e.getSource() == button4) {
            if (isEqualClicked) {
                displayLabel.setText("4");
            } else {
                displayLabel.setText(displayLabel.getText() + "4");
            }

        } else if (e.getSource() == button5) {
            if (isEqualClicked) {
                displayLabel.setText("5");
            } else {
                displayLabel.setText(displayLabel.getText() + "5");
            }

        } else if (e.getSource() == button6) {
            if (isEqualClicked) {
                displayLabel.setText("6");
            } else {
                displayLabel.setText(displayLabel.getText() + "6");
            }

        } else if (e.getSource() == button3) {
            if (isEqualClicked) {
                displayLabel.setText("3");
            } else {
                displayLabel.setText(displayLabel.getText() + "3");
            }

        } else if (e.getSource() == button2) {
            if (isEqualClicked) {
                displayLabel.setText("2");
            } else {
                displayLabel.setText(displayLabel.getText() + "2");
            }

        } else if (e.getSource() == button1) {
            if (isEqualClicked) {
                displayLabel.setText("1");
            } else {
                displayLabel.setText(displayLabel.getText() + "1");
            }

        } else if (e.getSource() == button0) {
            if (isEqualClicked) {
                displayLabel.setText("0");
            } else {
                displayLabel.setText(displayLabel.getText() + "0");
            }

        } else if (e.getSource() == decimalButton) {
            if (isEqualClicked) {
                displayLabel.setText("0.");
            } else {
                if (displayLabel.getText().equals("")) {
                    displayLabel.setText(displayLabel.getText() + "0.");
                } else {
                    displayLabel.setText(displayLabel.getText() + ".");
                }
            }

//Function of Operators

        } else if (e.getSource() == plusButton) {
            numbers.add(displayLabel.getText());
            numbers.add("+");
            displayLabel.setText("");

        } else if (e.getSource() == minusButton) {
            numbers.add(displayLabel.getText());
            numbers.add("-");
            displayLabel.setText("");

        } else if (e.getSource() == divideButton) {
            numbers.add(displayLabel.getText());
            numbers.add("/");
            displayLabel.setText("");

        } else if (e.getSource() == multiplyButton) {
            numbers.add(displayLabel.getText());
            numbers.add("*");
            displayLabel.setText("");

        } else if (e.getSource() == equalsButton) {
            numbers.add(displayLabel.getText());
            for (int i = 0; i < numbers.size() - 1; i++) {
                if (numbers.get(i).equals("+")) {
                    numbers.set(i + 1, Float.toString(Float.parseFloat(String.valueOf(numbers.get(i - 1))) + Float.parseFloat(String.valueOf(numbers.get(i + 1)))));
                } else if (numbers.get(i).equals("-")) {
                    numbers.set(i + 1, Float.toString(Float.parseFloat(String.valueOf(numbers.get(i - 1))) - Float.parseFloat(String.valueOf(numbers.get(i + 1)))));
                } else if (numbers.get(i).equals("*")) {
                    numbers.set(i + 1, Float.toString(Float.parseFloat(String.valueOf(numbers.get(i - 1))) * Float.parseFloat(String.valueOf(numbers.get(i + 1)))));
                } else if (numbers.get(i).equals("/")) {
                    numbers.set(i + 1, Float.toString(Float.parseFloat(String.valueOf(numbers.get(i - 1))) / Float.parseFloat(String.valueOf(numbers.get(i + 1)))));
                }
            }
            displayLabel.setText(numbers.get(numbers.size() - 1));
            isEqualClicked = true;
        }
    }
    }


